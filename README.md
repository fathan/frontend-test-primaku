## Project Information


Hello I'm Fathan. this project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app). Please code review all code mas Paiman.


Repository: https://gitlab.com/fathan/frontend-test-primaku

----

## Technology Stack

```bash
- TypeScript
- React.js 18
- Next.js 13
- Redux for State Management
- TailwindCSS 3.3.2
```

## Package List

```bash
{
  "name": "frontend-test-primaku",
  "version": "0.1.0",
  "private": true,
  "scripts": {
    "dev": "next dev",
    "build": "next build",
    "start": "next start",
    "lint": "next lint",
    "test:unit": "jest --watch --verbose",
    "test:ci": "jest --ci"
  },
  "dependencies": {
    "@reduxjs/toolkit": "1.9.5",
    "@types/node": "20.3.3",
    "@types/react": "18.2.14",
    "@types/react-dom": "18.2.6",
    "@types/react-redux": "7.1.25",
    "@types/redux": "3.6.0",
    "@types/redux-thunk": "2.1.0",
    "autoprefixer": "10.4.14",
    "axios": "1.4.0",
    "dotenv": "16.3.1",
    "eslint": "8.44.0",
    "eslint-config-next": "13.4.8",
    "lodash": "4.17.21",
    "next": "13.4.8",
    "next-redux-wrapper": "8.1.0",
    "next-seo": "6.1.0",
    "postcss": "8.4.24",
    "react": "18.2.0",
    "react-dom": "18.2.0",
    "react-icons": "4.10.1",
    "react-redux": "8.1.1",
    "tailwindcss": "3.3.2",
    "typescript": "5.1.6"
  },
  "devDependencies": {
    "@types/lodash": "4.14.195",
    "@types/qs": "6.9.7",
    "qs": "6.11.2",
    "react-toastify": "9.1.3"
  }
}

```

## Getting Started

First, run the development server:

```bash
$ npm run dev
# or
$ yarn dev
# or
$ pnpm dev
```

For build to production run:
```bash
$ npm run build
```

For run Unit Testing:
```bash
$ npm run test:unit
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

------

## Project Structure

```bash
.next
node_modules
public
src
  @types
  assets
  components
  layouts
  pages
  redux
  services
  utils
.env
.eslintrc.json
.gitignore
next-env.d.ts
next-config.json
package-lock.json
package.json
postcss.config.js
README.md
tailwind.config.js
tsconfig.json

```
----

## Production
- Project has been deployed in Vercel. please check this link: [`https://tmdb-next-kohl.vercel.app`](https://tmdb-next-kohl.vercel.app).

----

## Screenshot projects
----

## Searching Movie Page Mobile view<br>
![](./src/assets/images/m1.png)

## Movie Page Mobile view<br>
![](./src/assets/images/m2.png)

## Home Web<br>
![](./src/assets/images/ss1.png)

## Movies Detail Web<br>
![](./src/assets/images/ss2.png)

## TV Series Detail Web<br>
![](./src/assets/images/ss3.png)


I m very happy. if I joined with engineering team frontend Primaku. 


Thank You,
Fathan

<br>
<br>

## @TODO
- Unit Testing